var boardDiv = undefined;
var stacks = undefined;
var deltaTime = 16;
var movingBlock = undefined;
var BLOCKSPEED = 1800;
var cursor = undefined;
var fromLabel = undefined;
var toLabel = undefined;
var selectedStack = undefined;

function main() {
	if (document.body != undefined) {
		initBoard();
		// fix glitch with fake mouse move
		boardMouseMove({"clientX": 300, "clientY" : 300});
		window.setInterval(function(){runPgm();},deltaTime);
	}
}

function initBoard() {
	boardDiv = document.createElement("DIV");
	boardDiv.id = "board";
	document.body.appendChild(boardDiv);

	stacks = [];
	stacks[0] = document.createElement("DIV");
	stacks[0].id = "leftStack";
	stacks[0].className = "stack";
	stacks[0].style.left = "50px";
	stacks[0].style.top = "150px";
	stacks[0].style.backgroundColor = "#C77";
	stacks[0].style.borderColor = "#833";
	stacks[0].blocks = [];
	boardDiv.appendChild(stacks[0]);

	stacks[1] = document.createElement("DIV");
	stacks[1].id = "midStack";
	stacks[1].className = "stack";
	stacks[1].style.left = "300px";
	stacks[1].style.top = "150px";
	stacks[1].style.backgroundColor = "#CCC";
	stacks[1].style.borderColor = "#333";
	stacks[1].blocks = [];
	boardDiv.appendChild(stacks[1]);

	stacks[2] = document.createElement("DIV");
	stacks[2].id = "rightStack";
	stacks[2].className = "stack";
	stacks[2].style.left = "550px";
	stacks[2].style.top = "150px";
	stacks[2].style.backgroundColor = "#7C7";
	stacks[2].style.borderColor = "#383";
	stacks[2].blocks = [];
	boardDiv.appendChild(stacks[2]);

	
	for (var i = 0; i < 4; ++i) {
		var block = makeBlock(100 + (15 * (3 - i)));
		block.style.left = (150 - (block.width / 2)) + "px";
		block.style.top = (450 - 50 - (75 * i)) + "px";
		stacks[0].blocks.push(block);
		boardDiv.appendChild(block);
	}

	cursor = document.createElement("DIV");
	cursor.id = "cursor";
	cursor.innerHTML = "FROM";
	cursor.className = "labelBlock";
	boardDiv.appendChild(cursor);
	addEvent(board,"mousemove",boardMouseMove);
	addEvent(board,"click",boardMouseClick);

	fromLabel = document.createElement("DIV");
	fromLabel.id = "fromLabel";
	fromLabel.innerHTML = "FROM";
	fromLabel.className = "labelBlock";
	boardDiv.appendChild(fromLabel);
	
	toLabel = document.createElement("DIV");
	toLabel.id = "toLabel";
	toLabel.innerHTML = "TO";
	toLabel.className = "labelBlock";
	boardDiv.appendChild(toLabel);
	
}

function makeBlock(size) {
	var block = document.createElement("DIV");
	block.className = "block";
	block.style.width = (size - 4) + "px";
	block.width = size;
	block.height = 50;
	return block;
}

function runPgm() {

	// is the program moving a block?
	if (movingBlock != undefined) {

		// remove the current animation goal from the "path stack"
		var target = movingBlock.points.pop();
		// calc per frame speed
		var speed = movingBlock.speed / (1000.0 / deltaTime);

		// copy the block's x and y position from the style sheet, parse to int and
		// adjust to the center of the block
		var x = parseInt(movingBlock.style.left,10);
		var y = parseInt(movingBlock.style.top,10);
		x = x + (movingBlock.width / 2);
		y = y + (movingBlock.height / 2);

		// is the distance left to go to the animation goal greater than the distance
		// the block will travel in this frame?
		if ( Math.sqrt( Math.pow(x - target.x,2) + Math.pow(y - target.y,2)) > speed) {

			// calc delta displacement for this frame
			var dir = [];
			dir.x = target.x - x;
			dir.y = target.y - y;

			// normalize the vector
			dir.mag = Math.sqrt((dir.x * dir.x) + (dir.y * dir.y));
			dir.x = dir.x / dir.mag;
			dir.y = dir.y / dir.mag;

			// scale it for speed
			dir.x = dir.x * speed;
			dir.y = dir.y * speed;

			// add the delta displacement to the current position and round it
			dir.x = Math.round(dir.x + x);
			dir.y = Math.round(dir.y + y);

			// insert the new position into the style sheet
			// dont forget to adjust from center to top left
			movingBlock.style.left = (dir.x - (movingBlock.width / 2)) + "px";
			movingBlock.style.top =  (dir.y - (movingBlock.height / 2)) + "px";

			// push the current target back onto the "path stack" because it hasnt been reached yet
			movingBlock.points.push(target);
		} else {
			// insert the target's position into the style sheet for the block's position
			// dont forget to adjust from center to top left
			movingBlock.style.left = (target.x - (movingBlock.width / 2)) + "px";
			movingBlock.style.top =  (target.y - (movingBlock.height / 2)) + "px";

			// since the block reached its goal, no need to push it back onto the stack

			// if there are no points left to go to, then there's no more need to animate this thing
			if (movingBlock.points.length < 1) {
				movingBlock = undefined;
				toLabel.style.visibility = "hidden";
				fromLabel.style.visibility = "hidden";
				cursor.style.visibility = "visible";
				if (checkForLoss()) {
					alert("Congrats, you lose.");
					resetGame();
				} else if (checkForVictory()) {
					alert("Congrats, you win.");
					resetGame();
				}
			}
		}
	}
		
}

function getPath(stackStart, stackEnd, stackPos) {
	var path = [];

	var startStackX = 150 + (250 * stackStart);
	var endStackX = 150 + (250 * stackEnd);
	var endStackY = 425 - (75 * stackPos);
	var heightAboveStack = 100;

	path.push({"x" : endStackX, "y" : endStackY});
	path.push({"x" : endStackX, "y" : heightAboveStack});
	path.push({"x" : startStackX, "y" : heightAboveStack});

	return path;
}

function moveBlock(startStack, destStack) {
	if (startStack == destStack){return;}

	var start = stacks[startStack];
	var dest = stacks[destStack];

	
	if (start.blocks.length < 1) {return;}

	movingBlock = start.blocks.pop();
	movingBlock.speed = BLOCKSPEED;
	movingBlock.points = getPath(startStack, destStack, dest.blocks.length);
	dest.blocks.push(movingBlock);
}

function boardMouseMove(event) {
	event = event || window.event;
	
	var x = event.clientX;
	var y = event.clientY;
	var rect = boardDiv.getBoundingClientRect();
	x = x - rect.left;
	y = y - rect.top;

	if (x < 275) {
		cursor.style.left = "75px";
	} else if (x > 275 && x < 525) {
		cursor.style.left = "325px";
	} else if (x > 525) {
		cursor.style.left = "575px";
	}
	cursor.style.top = "500px";
	
}

function boardMouseClick(event) {
	var rect = boardDiv.getBoundingClientRect();
	event = event || window.event;
	var x = event.clientX - rect.left;
	var y = event.clientY - rect.top;

	if (movingBlock != undefined) {return;}

	var ss = -1;
	if (x < 275) {
		ss = 0;
	} else if (x > 275 && x < 525) {
		ss = 1;
	} else if (x > 525) {
		ss = 2;
	}

	if (selectedStack == undefined) {
		if (stacks[ss].blocks.length > 0) {
			selectedStack = ss;
			fromLabel.style.visibility = "visible";
			fromLabel.style.left = 75 + (selectedStack * 250) + "px";
			cursor.innerHTML = "TO";
		}
	} else {
		if (selectedStack == ss) {
			toLabel.style.visibility = "hidden";
			fromLabel.style.visibility = "hidden";
			cursor.style.visibility = "visible";
			cursor.innerHTML = "FROM";
			selectedStack = undefined;
		} else {
			toLabel.style.visibility = "visible";
			toLabel.style.left = 75 + (ss * 250) + "px";
			moveBlock(selectedStack,ss);
			cursor.style.visibility = "hidden";
			cursor.innerHTML = "FROM";
			selectedStack = undefined;
		}
	}
}

function addEvent(elem, evtName, func) {
	if (elem.attachEvent != undefined) {
		elem.attachEvent("on" + evtName, func);
	} else if (elem.addEventListener != undefined) {
		elem.addEventListener(evtName,func,false);
	} else {
		elem["on" + evtName] = func;
	}
}

function checkForLoss() {
	for (var i = 0; i < 3; ++i) {
		var prevSize = 300;
		for (var j = 0; j < stacks[i].blocks.length; ++j) {
			if (stacks[i].blocks[j].width < prevSize) {
				prevSize = stacks[i].blocks[j].width;
			} else {
				return true;
			}
		}
	}
	return false;
}

function checkForVictory() {
	return stacks[2].blocks.length === 4;
}

function resetGame() {
	location.reload(true);
}
